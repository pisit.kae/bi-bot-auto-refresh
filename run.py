from PIL import Image
import os
import time

import pyautogui
import pandas as pd


for x in range(3):
    time.sleep(1)
    print('waiting ' + str(x+1))


while (True):

    path = "input/serverName"
    dir_list = os.listdir(path)
    print("Files and directories in '", path, "' :")
    for pic in dir_list:

        pyautogui.press('home')
        picLocation = pyautogui.locateOnScreen('input/serverName/' + pic, confidence=0.95)

        while picLocation is None:
            print('cannot find ' + pic)
            pyautogui.press('down')
            time.sleep(1)
            picLocation = pyautogui.locateOnScreen('input/serverName/' + pic, confidence=0.95)

        pyautogui.moveTo(picLocation[0]+10, picLocation[1]+10)
        time.sleep(2)
        refreshBtn = pyautogui.locateOnScreen('input/refreshBtn.png', confidence=0.9)

        while refreshBtn is None:
            print('cannot find refreshBtn')
            time.sleep(1)
            refreshBtn = pyautogui.locateOnScreen('input/refreshBtn.png', confidence=0.9)

        pyautogui.moveTo(refreshBtn[0]+10, refreshBtn[1]+10)
        time.sleep(0.5)
        pyautogui.click()
    print('done')
    time.sleep(2)  # 60 * minute
